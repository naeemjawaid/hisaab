//
//  MidribView.swift
//  hisaab
//
//  Created by Muhammad Naeem Jawaid on 09/07/2018.
//  Copyright © 2018 microTek. All rights reserved.
//

import UIKit

class MidribView: UIView {
    
    //MARK:- Properties
    weak var midribViewController: MidribViewController? {
        didSet {
            setupViews()
        }
    }
    
    //MARK:- Views setup
    private func setupViews() {
        if let midribViewController = midribViewController {
            midribViewController.view.addSubview(self)
            NSLayoutConstraint.activate([
                self.leadingAnchor.constraint(equalTo: midribViewController.view.leadingAnchor),
                self.topAnchor.constraint(equalTo: midribViewController.view.topAnchor),
                self.trailingAnchor.constraint(equalTo: midribViewController.view.trailingAnchor),
                self.bottomAnchor.constraint(equalTo: midribViewController.view.bottomAnchor)])
        }
    }
}
