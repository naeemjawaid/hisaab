//
//  ViewController.swift
//  hisaab
//
//  Created by Muhammad Naeem Jawaid on 09/07/2018.
//  Copyright © 2018 microTek. All rights reserved.
//

import UIKit

class MidribViewController: UIViewController {
    
    //MARK:- Properties
    let midribView: MidribView = {
        let midribView = MidribView()
        midribView.backgroundColor = .red
        midribView.translatesAutoresizingMaskIntoConstraints = false
        return midribView
    }()
    
    //MARK:- View appearance
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //MARK:- View setup
    private func setupView() {
        midribView.midribViewController = self
    }
}

